def subset_sum(arr, n, l, t, lst):
    if l == 0:
        for j in lst:
            print (" ", arr[j], end='')
        print ("\n------------------\n")
        return 0

    for i in range(t, len(arr)):
        if l - arr[i] >= 0 and i not in lst:
            l = l - arr[i]
            if subset_sum(arr, n, l, i, lst + [i]):
                return 1
            else:
                l = l + arr[i]
    return 0

if __name__ == "__main__":
    n = 10
    arr = [1, 3, 7, 3, 5, 2, 8, 4, 7, 9]
    subset_sum(arr, n, n, 0, [])
