def isParenthesis(c):
	return c == '(' or c == ')'

def isValidString(st_):
	cnt = 0
	for i in range(len(st_)):
		if st_[i] == "(":
			cnt += 1
		elif st_[i] == ")":
			cnt -= 1
		if cnt < 0:
			return False
	return cnt == 0

def removeinvalidparentheses(st_):
	if len(st_) == 0:
		return 

	visit = set()

	q = []
	temp = 0
	level = 0

	q.append(st_)
	visit.add(st_)
	while(len(q)):
		st_ = q[0]
		q.pop()
		if isValidString(st_):
			print (st_)
			level = True
		if level:
			continue
		for i in range(len(st_)):
			if not isParenthesis(st_[i]):
				continue

			temp = st_[0:i] + st_[i+1:]
			if temp not in visit:
				q.append(temp)
				visit.add(temp)


expression = "()())()"
removeinvalidparentheses(expression) 
expression = "()v)"
removeinvalidparentheses(expression) 


