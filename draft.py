import numpy as np 

P = np.array([[1, 2, 2],
	          [2, 4, 4],
	          [2, 4, 4]])/9

print (np.linalg.matrix_power(P, 5))
print (P)
print (np.diagonal(P).sum())