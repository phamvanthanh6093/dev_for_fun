import numpy as np 
import sys

def check(board, x, y, v):
	if v in board[x, :] or v in board[:, y]:
		return False
	i, j = (x//3)*3, (y//3)*3
	if v in board[i:i+3, j:j+3]:
		return False

	return True

def sudoku(board, x, y):
	# print (board, x, y, len(np.where(board == 0)[0]))
	# print ("-----------------------")
	if 0 not in board:
		return 1
	if 0 in board and (x == board.shape[0]-1 and y == board.shape[1]):
		return 0

	if y == board.shape[1]:
		y = 0
		x += 1
	if board[x, y] != 0:
		return sudoku(board, x, y+1)

	for v in range(1,10):
		if check(board, x, y, v):
			board[x, y] = v
			if sudoku(board, x, y+1):
				return 1
			else:
				board[x, y] = 0
	
	return 0


if __name__ == "__main__":
	n = 9
	# board = np.zeros((n, n))
	board = np.array([[9, 1, 7, 2, 5, 4, 0, 0, 0], 
         			  [4, 0, 2, 0, 8, 0, 0, 0, 0], 
         			  [6, 5, 0, 0, 0, 3, 4, 0, 0], 
         			  [0, 0, 3, 0, 9, 0, 2, 5, 6], 
         			  [5, 0, 0, 7, 0, 0, 3, 0, 9], 
         			  [2, 0, 0, 0, 0, 5, 0, 7, 1], 
         			  [0, 2, 0, 5, 3, 0, 7, 6, 0], 
         			  [3, 7, 0, 1, 6, 0, 0, 9, 8], 
         			  [0, 0, 0, 0, 0, 0, 0, 3, 0]])
	# print (len(np.where(board == 0)[0]))
	if sudoku(board, 0, 0):
		print (board)
	else:
		print ("Cannot solve!")
	# print (board)
