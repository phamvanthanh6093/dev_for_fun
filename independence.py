from scipy.stats import norm
import numpy as np 

x1 = norm.rvs(3, 2, size=1000)
x2 = norm.rvs(10, 4, size=1000)
print (np.mean(x1))
print (np.mean(x2))
print (np.mean(x1*x2))