import numpy as np 

def check(graph, lst, idx, c):
	for j in range(graph.shape[1]):
		if graph[idx][j]:
			if lst[j] == c:
				return False
	return True

def color(graph, lst, idx, m):
	if idx == graph.shape[0]:
		print (lst)
		return 1
	for i in range(1, m+1):
		if check(graph, lst, idx, i):
			lst[idx] = i
			if color(graph, lst, idx+1, m):
				return 1
			else:
				lst[idx] = 0
	return 0

if __name__ == "__main__":
	graph = np.array([[0, 1, 0, 0, 1, 1],
		              [1, 0, 1, 1, 1, 0],
		              [0, 1, 0, 1, 0, 1],
		              [0, 1, 1, 0, 0, 0],
		              [1, 1, 0, 0, 0, 1],
		              [1, 0, 1, 0, 1, 0]])

	m = 3
	lst = [0]*graph.shape[0]
	color(graph, lst, 0, 3)
